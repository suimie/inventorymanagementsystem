﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    class SalesPreference
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public Decimal SumOfSold { get; set; }
    }
}
