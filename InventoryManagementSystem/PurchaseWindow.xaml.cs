﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for PurchaseWindow.xaml
    /// </summary>
    public partial class PurchaseWindow : Window
    {
        Product currProduct;
        public PurchaseWindow(Product product)
        {
            currProduct = product;

            InitializeComponent();
            Canvas.SetLeft(this, System.Windows.SystemParameters.PrimaryScreenWidth / 2 - 350);
            Canvas.SetTop(this, System.Windows.SystemParameters.PrimaryScreenHeight / 2 - 170);

            tbQuantity.Focus();
            tbCode.IsEnabled = false;
            tbName.IsEnabled = false;
            tbDesp.IsEnabled = false;
            tbSupplier.IsEnabled = false;
            tbCategory.IsEnabled = false;
            tbPrice.IsEnabled = false;
            tbMinLevel.IsEnabled = false;
            tbReceived.IsEnabled = false;
            tbSold.IsEnabled = false;
            tbOnHand.IsEnabled = false;

            tbCode.Text = product.Id + "";
            tbName.Text = product.Name;
            tbDesp.Text = product.Description;
            tbSupplier.Text = product.SupplierName;
            tbCategory.Text = product.Category;
            tbPrice.Text = product.Price.ToString();
            tbMinLevel.Text = product.MinLevel + "";
            tbReceived.Text = product.Received + "";
            tbSold.Text = product.Sold + "";
            tbOnHand.Text = product.OnHand + "";
            dpPurchase.SelectedDate = DateTime.Now;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btPurchase_Click(object sender, RoutedEventArgs e)
        {
            if (tbQuantity.Text == "") return;

            try
            {
                if (int.TryParse(tbQuantity.Text, out int quantity))
                {
                    DateTime pDate = (DateTime)dpPurchase.SelectedDate;
                    Purchase purchase = new Purchase(0, currProduct.SupplierId, currProduct.Id, quantity, pDate);

                    Database.getInstance().AddPurchase(purchase);

                    DialogResult = true;
                }
                else
                {
                    MessageBox.Show("Invalid Quantity.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Error inserting database: " + ex.Message);
            }


        }

        private void tbQuantity_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = false;
                btPurchase_Click(null, null);
            }
        }
    }
}
