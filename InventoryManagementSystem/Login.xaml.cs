﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            
            
            Canvas.SetLeft(this, System.Windows.SystemParameters.PrimaryScreenWidth / 2);
            Canvas.SetTop(this, System.Windows.SystemParameters.PrimaryScreenHeight /2);
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btLogin_Click(object sender, RoutedEventArgs e)
        {
            string id = tbId.Text;
            string pwd = pbPassword.Password;

            if(Utility.Login(id, pwd))
            {
                DialogResult = true;
            }
            else
            {
                DialogResult = false;
            }
        }

        private void pbPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((dynamic)this.DataContext).SecurePassword = ((PasswordBox)sender).SecurePassword; }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender == this && e.LeftButton == MouseButtonState.Pressed)
            {
                e.Handled = true;

                Point p = e.GetPosition(this);
                if (p.X <= 205)
                {
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        return;
                    }
                }
                e.Handled = false;
            }
        }
    }
}
