﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using System.Data.SqlClient;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for PreferenceChartControl.xaml
    /// </summary>
    public partial class PreferenceChartControl : UserControl
    {
        public PlotModel Model { get; set; }
        List<SalesPreference> PerferencesList;
        Decimal Max;

        public PreferenceChartControl()
        {
            InitializeComponent();

            DateTime today = DateTime.Now;

            // initially, show chart for since 1 month before from today
            dpFrom.SelectedDate = today.AddDays(-30);
            dpTo.SelectedDate = today;
            cmbCategory.SelectedIndex = 0;

            btSeeTop20_Click(null, null);
        }

        private void QueryData()
        {
            Utility.CATEGORY c = (Utility.CATEGORY)cmbCategory.SelectedIndex;
            string category = Enum.GetName(c.GetType(), c);
            //string category = cmbCategory.Text;
            DateTime from = (DateTime)dpFrom.SelectedDate;
            DateTime to = (DateTime)dpTo.SelectedDate;

            try
            {
                Max = Database.getInstance().GetMaxOfByPeriodNCategory(from, to, category);

                bool isLimit = (bool)rbBarChart.IsChecked;

                List<SalesPreference> list = Database.getInstance().GetSalesListByPeriodNCategory(from, to, category, isLimit);
                PerferencesList = (from r in list orderby r.ProductId descending select r).ToList();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error selecting database: " + e.Message);
            }

        }

        private PlotModel CreateBarChart()
        {
            var plot = new PlotModel
            {
                Title = "Preferences",
                Subtitle = "Top 20 Products Sold Bar Chart"
            };
            CategoryAxis yaxis = new CategoryAxis();
            yaxis.Position = AxisPosition.Left;
            yaxis.MajorGridlineStyle = LineStyle.Dot;
            yaxis.MinorGridlineStyle = LineStyle.Dot;
            foreach (SalesPreference sp in PerferencesList)
            {
                yaxis.Labels.Add(sp.ProductName+"(" + sp.ProductId + ")");
            }

            LinearAxis xaxis = new LinearAxis();
            xaxis.Position = AxisPosition.Bottom;
            xaxis.Minimum = 0;
            xaxis.Maximum = ((double)Max > 100) ? (double)Max + 30 : 130;
            xaxis.MajorStep = 20;
            xaxis.MinorStep = 5;
            xaxis.TickStyle = TickStyle.Inside;
            xaxis.MajorGridlineStyle = LineStyle.Solid;
            xaxis.MinorGridlineStyle = LineStyle.Dot;

            BarSeries barSeries = new BarSeries();
            foreach (SalesPreference sp in PerferencesList)
            {
                BarItem b = new BarItem();
                b.Value = (double)sp.SumOfSold;
                barSeries.Items.Add(b);
            }

            plot.Axes.Add(xaxis);
            plot.Axes.Add(yaxis);
            plot.Series.Add(barSeries);

            return plot;
        }

        private PlotModel CreatePieChart()
        {
            var plot = new PlotModel
            {
                Title = "Preferences",
                Subtitle = "All Products Sold Pie Chart"
            };
            dynamic seriesP1 = new PieSeries { StrokeThickness = 2.0, InsideLabelPosition = 0.8, AngleSpan = 360, StartAngle = 0 };

            foreach (SalesPreference sp in PerferencesList)
            {
                seriesP1.Slices.Add(new PieSlice(sp.ProductName + "(" + sp.ProductId + ")", (double)sp.SumOfSold));
            }
            plot.Series.Add(seriesP1);
            return plot;
        }

        public void btSeeTop20_Click(object sender, RoutedEventArgs e)
        {
            QueryData();

            if (rbBarChart.IsChecked == true)
            {
                plotViewPreferences.Model = CreateBarChart();
            }
            else
            {
                plotViewPreferences.Model = CreatePieChart();
            }
            plotViewPreferences.Model.InvalidatePlot(true);
        }
   

        private void rbBarChart_Click(object sender, RoutedEventArgs e)
        {
            QueryData();

            plotViewPreferences.Model = CreateBarChart();
            plotViewPreferences.Model.InvalidatePlot(true);
        }

        private void rbPieChart_Click(object sender, RoutedEventArgs e)
        {
            QueryData();

            plotViewPreferences.Model = CreatePieChart();
            plotViewPreferences.Model.InvalidatePlot(true);
        }

        private void cmbCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btSeeTop20_Click(null, null);
        }
    }
}
