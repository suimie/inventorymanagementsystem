﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class Purchase
    {
        public int Id { get; set; }
        public int SupplierId { get; set; }
        public int ProductId { get; set; }
        public int Received { get; set; }
        public DateTime PurchaseDate { get; set; }

        public Purchase(int id, int sid, int pid)
        {
            Id = id;
            SupplierId = sid;
            ProductId = pid;
        }
        public Purchase(int id, int sid, int pid, int received, DateTime pdate)
        {
            Id = id;
            SupplierId = sid;
            ProductId = pid;
            Received = received;
            PurchaseDate = pdate;
        }
    }
}
