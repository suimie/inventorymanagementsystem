﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for ManageSalesControl.xaml
    /// </summary>
    public partial class ManageSalesControl : UserControl
    {
        public List<Sales> salesList = new List<Sales>();
        public static int ordering = 1;

        bool isSaved = false;
        double Subtotal;
        double TPS;
        double TVQ;
        double Total;
        int SID;

        public ManageSalesControl()
        {
            InitializeComponent();
            dpSalesDate.SelectedDate = DateTime.Now;
            refreshSalesList();
        }

        private void refreshSalesList()
        {
            lvSales.ItemsSource = salesList;
            lvSales.Items.Refresh();
            tbProduct.Text = "";

            Subtotal = 0;
            foreach (Sales s in salesList)
            {
                Subtotal += (double)s.Amount;
            }
            TPS = Subtotal * 0.05;
            TVQ = Subtotal * 0.0975;
            Total = Subtotal + TPS + TVQ;

            lblSubTotal.Content = Subtotal.ToString("C", new CultureInfo("en-CA"));
            lblTPS.Content = TPS.ToString("C", new CultureInfo("en-CA"));
            lblTVQ.Content = TVQ.ToString("C", new CultureInfo("en-CA"));
            lblTotal.Content = Total.ToString("C", new CultureInfo("en-CA"));

            tbProduct.Focus();
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            if(isSaved == false && salesList.Count > 0)
            {
                if(MessageBox.Show("You didn't save the sales info. Do you want to save?", "Message", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    btSave_Click(null, null);
                }
            }
            ordering = 1;
            for(int i=salesList.Count-1; i >= 0; i--)
            {
                salesList.RemoveAt(i);
            }

            refreshSalesList();
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {

            if (MessageBox.Show("Are you sure you want to delete?", "Delete Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            else
            {
                //do yes stuff
                int selectedIdx = lvSales.SelectedIndex;

                if (selectedIdx < 0) return;

                salesList.RemoveAt(selectedIdx);

                lvSales.Items.Refresh();
                //added by Valini to fix total price after deleting an item from list
                refreshSalesList();

            }
        }

        private void Print_Click(object sender, RoutedEventArgs e)
        {

            string DestinationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            DestinationPath += @"\..\..\PDFRECEIPT\";
         
            string filename = String.Format("{0}{1:0000}{2:00}{3:00}_{4}.pdf", DestinationPath, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, SID);
            FileStream fs = new FileStream(filename, FileMode.Create);

            // Create an instance of the document class which represents the PDF document itself.
            iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4, 25, 25, 30, 30);
            // Create an instance to the PDF file by creating an instance of the PDF 
            // Writer class using the document and the filestrem in the constructor.

            iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, fs);
          
            // Open the document to enable you to write to the document

            document.Open();
            
            //document.Add(new iTextSharp.text.Paragraph("CUSTOMER RECEIPT"));
            
            PdfPTable table = new PdfPTable(4);

            //leave a gap before and after the table

            table.SpacingBefore = 20f;

            table.SpacingAfter = 30f;
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
            Font sboldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
            Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);
            //first line
            PdfPCell cell = new PdfPCell(new Phrase("CUSTOMER RECEIPT", boldFont));
            cell.BackgroundColor = new BaseColor(196, 231, 234);
            cell.Colspan = 4;
            cell.Border = 0;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);

            //second line
            PdfPCell cell21 = new PdfPCell(new Phrase("Date:", sboldFont));
            cell21.Border = 0;
            //table.AddCell("Date:");
            table.AddCell(cell21);

            PdfPCell cell22 = new PdfPCell(new Phrase(DateTime.Now.ToShortDateString(), normalFont));
            cell22.Border = 0;
            //table.AddCell(DateTime.Now.ToShortDateString());
            table.AddCell(cell22);

            PdfPCell cell23 = new PdfPCell(new Phrase("Time:", sboldFont));
            cell23.Border = 0;
            //table.AddCell("Time:");
            table.AddCell(cell23);

            PdfPCell cell24 = new PdfPCell(new Phrase(DateTime.Now.ToLongTimeString(), normalFont));
            cell24.Border = 0;
            //table.AddCell(DateTime.Now.ToLongTimeString());
            table.AddCell(cell24);



            //Add headers

            PdfPCell cell31 = new PdfPCell(new Phrase("Product ID", sboldFont));
            table.AddCell(cell31);
            PdfPCell cell32 = new PdfPCell(new Phrase("Product Name", sboldFont));
            table.AddCell(cell32);
            //table.AddCell("Product ID");
            // table.AddCell("Product Name");
            PdfPCell cell33 = new PdfPCell(new Phrase("Quantity", sboldFont));
            table.AddCell(cell33);
            //table.AddCell("Quantity");

            PdfPCell cell34 = new PdfPCell(new Phrase("Amount in $", sboldFont));
            table.AddCell(cell34);
          //  table.AddCell("Amount in $");


            foreach (Sales item in salesList)
            {
                table.AddCell(item.ProductId.ToString());
                table.AddCell(item.ProductName.ToString());
                table.AddCell(item.QuantitySold.ToString());
                table.AddCell( item.Amount.ToString("C", new CultureInfo("en-CA")));

            }
            
            document.Add(table);
            //add table for totals

            PdfPTable tableBill = new PdfPTable(2);
            tableBill.DefaultCell.Border = 0;

            tableBill.AddCell("Subtotal:");
            tableBill.AddCell(Subtotal.ToString("C", new CultureInfo("en-CA")));

            tableBill.AddCell("TPS (5%):");
            tableBill.AddCell(TPS.ToString("C", new CultureInfo("en-CA")));

            tableBill.AddCell("TVQ (9.75%):");
            tableBill.AddCell(TVQ.ToString("C", new CultureInfo("en-CA")));

            tableBill.AddCell("Total:");
            tableBill.AddCell(Total.ToString("C", new CultureInfo("en-CA")));

            document.Add(tableBill);
            // Close the document

            document.Close();
            // Close the writer instance

            writer.Close();
            // Always close open filehandles explicity
            fs.Close();


            MessageBox.Show("Your pdf receipt has been successfully created.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);


        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            if (lvSales.Items.Count == 0) {

                MessageBox.Show("Please press enter to order product.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            DateTime salesDate = (DateTime)dpSalesDate.SelectedDate;

            SID = Database.getInstance().AddSales(salesDate, Math.Round(Subtotal * 100)/100, Math.Round(TPS * 10000)/10000, Math.Round(TVQ * 10000)/10000, Math.Round(Total * 100)/100);

            foreach(Sales s in salesList)
            {
                Database.getInstance().AddSalesDetailLine(SID, s);
            }

            Utility.mainWindow.RefreshReminders();

            isSaved = true;

            Print_Click(null, null);
      
            btClear_Click(null, null);
        }



        private void tbProduct_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = false;

                if ((rbProductCode.IsChecked == true) && (int.TryParse(tbProduct.Text, out int id)))
                {
                    Product product = Database.getInstance().GetProductbyId(id);

                    if (product == null)
                    {
                        MessageBox.Show("The product [" + id + "] does not exist.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                        tbProduct.Text = "";
                        return;
                    }

                    SalesDlg dlg = new SalesDlg(product, salesList);
                    if (dlg.ShowDialog() == true)
                    {
                        refreshSalesList();
                      
                    }
                }
                else if (rbProductName.IsChecked == true)
                {
                    string name = tbProduct.Text;

                    Product product = Database.getInstance().GetProductbyName(name);

                    if (product == null)
                    {
                        MessageBox.Show("The product [" + name + "] does not exist.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                        tbProduct.Text = "";
                        return;

                    }
                    SalesDlg dlg = new SalesDlg(product, salesList);
                    if (dlg.ShowDialog() == true)
                    {
                        refreshSalesList();
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Product Code entered.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    tbProduct.Text = "";
                    return;
                }

            }
        }

        private void btSearch_Click(object sender, RoutedEventArgs e)
        {

            if ((rbProductCode.IsChecked == true) && (int.TryParse(tbProduct.Text, out int id)))
            {
                Product product = Database.getInstance().GetProductbyId(id);

                if (product == null)
                {
                    MessageBox.Show("The product [" + id + "] does not exist.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    tbProduct.Text = "";
                    return;
                }

                SalesDlg dlg = new SalesDlg(product, salesList);
                if (dlg.ShowDialog() == true)
                {
                    refreshSalesList();

                }
            }
            else if (rbProductName.IsChecked == true)
            {
                string name = tbProduct.Text;

                Product product = Database.getInstance().GetProductbyName(name);

                if (product == null)
                {
                    MessageBox.Show("The product [" + name + "] does not exist.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    tbProduct.Text = "";
                    return;

                }
                SalesDlg dlg = new SalesDlg(product, salesList);
                if (dlg.ShowDialog() == true)
                {
                    refreshSalesList();
                }
            }
            else
            {
                MessageBox.Show("Invalid Product Code entered.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                tbProduct.Text = "";
                return;
            }

        


        }
    }
}
