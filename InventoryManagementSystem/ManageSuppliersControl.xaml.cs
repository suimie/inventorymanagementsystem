﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Threading;
using System.ComponentModel;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for ManageSuppliersControl.xaml
    /// </summary>
    public partial class ManageSuppliersControl : UserControl
    {
        private Supplier currentSupplier;
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;
        List<Supplier> supplierList;

        public ManageSuppliersControl()
        {
            try
            {

                InitializeComponent();
                if (currentSupplier == null)
                {

                    btSave.Content = "Add New";
                }



                refreshSuppliersList();
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }

        private void refreshSuppliersList()
        {
            try { 


            supplierList = Database.getInstance().GetAllSuppliers();
            lvSuppliers.ItemsSource = supplierList;
            tbSuppSearch.Text = "";}

            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error selecting database: " + e.Message);
            }

        }

        private void lvSuppliers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lvSuppliers.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            btSave.Content = "Update";
            currentSupplier = (Supplier)lvSuppliers.Items[index];
            lblSuppId.Content = currentSupplier.Id + "";
            tbSuppName.Text = currentSupplier.Name;
            tbSuppAddr1.Text = currentSupplier.Address1;
            tbSuppAddr2.Text = currentSupplier.Address2;
            cmbSuppProvince.Text = currentSupplier.Province;
            tbSuppPostalCode.Text = currentSupplier.PostalCode;
            tbSuppPhone.Text = currentSupplier.Phone;
            tbSuppEmail.Text = currentSupplier.Email;
            cmbSuppCat.Text = currentSupplier.Category;


        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Supplier supplier = (currentSupplier == null ? new Supplier() : currentSupplier);


                supplier.Name = tbSuppName.Text;
                supplier.Address1 = tbSuppAddr1.Text;
                supplier.Address2 = tbSuppAddr2.Text;
                supplier.Province = cmbSuppProvince.Text;
                supplier.PostalCode = tbSuppPostalCode.Text;
                supplier.Phone = tbSuppPhone.Text;
                supplier.Email = tbSuppEmail.Text;
                supplier.Category = cmbSuppCat.Text;

                if (currentSupplier == null)
                {//add-insert

                    Database.getInstance().AddSupplier(supplier);
                }
                else
                {

                    Database.getInstance().UpdateSupplier(supplier);

                }
               
                refreshSuppliersList();


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);

            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Invalid data entered " + ex.Message);

            }

        }
        
        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete?", "Delete Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                Environment.Exit(0);
            }
            else
            {
                //do yes stuff
            

            int index = lvSuppliers.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Supplier supplier = (Supplier)lvSuppliers.Items[index];
            try
            {
                Database.getInstance().DeleteSupplierById(supplier.Id);
                refreshSuppliersList();
                btClear_Click(null, null);
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
            }

        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            lblSuppId.Content = "";
            tbSuppName.Text = "";
            tbSuppAddr1.Text = "";
            tbSuppAddr2.Text = "";
            cmbSuppProvince.Text = "";
            tbSuppPostalCode.Text = "";
            tbSuppPhone.Text = "";
            tbSuppEmail.Text = "";
            cmbSuppCat.Text = "";
            btSave.Content = "Add New";

        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            int index = lvSuppliers.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Supplier supplier = (Supplier)lvSuppliers.Items[index];
            try
            {

                supplier.Name = tbSuppName.Text;
                supplier.Name = tbSuppName.Text;
                supplier.Address1 = tbSuppAddr1.Text;
                supplier.Address2 = tbSuppAddr2.Text;
                supplier.Province = cmbSuppProvince.Text;
                supplier.PostalCode = tbSuppPostalCode.Text;
                supplier.Phone = tbSuppPhone.Text;
                supplier.Email = tbSuppEmail.Text;
                supplier.Category = cmbSuppCat.Text;
                //person.Age = int.Parse(tbAge.Text);

                Database.getInstance().UpdateSupplier(supplier);
                refreshSuppliersList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }

        }

        private void tbSuppSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Supplier> suppliersList = Database.getInstance().GetAllSuppliers();
            String word = tbSuppSearch.Text;
            if (word != "")
            {
                var result = from t in suppliersList where t.Name.Contains(word) select t;
                suppliersList = result.ToList();

            }
            lvSuppliers.ItemsSource = suppliersList;



        }

      

        private void Excel_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel Workbook|*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    ProgressIndicator.IsBusy = true;

                    Task.Factory.StartNew(() =>
                    {
                        Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                        Workbook wb = app.Workbooks.Add(XlSheetType.xlWorksheet);
                        Worksheet ws = (Worksheet)app.ActiveSheet;
                        app.Visible = false;
                        ws.Cells[1, 1] = "ID";
                        ws.Cells[1, 2] = "Product Name";
                        ws.Cells[1, 3] = "Address 1";
                        ws.Cells[1, 4] = "Address 2";
                        ws.Cells[1, 5] = "Province";
                        ws.Cells[1, 6] = "Postal Code";
                        ws.Cells[1, 7] = "Phone";
                        ws.Cells[1, 8] = "Email";
                        ws.Cells[1, 9] = "Category";


                        int i = 2;
                        foreach (Supplier item in supplierList)
                        {
                            ws.Cells[i, 1] = item.Id;
                            ws.Cells[i, 2] = item.Name;
                            ws.Cells[i, 3] = item.Address1;
                            ws.Cells[i, 4] = item.Address2;
                            ws.Cells[i, 5] = item.Province;
                            ws.Cells[i, 6] = item.PostalCode;
                            ws.Cells[i, 7] = item.Phone;
                            ws.Cells[i, 8] = item.Email;
                            ws.Cells[i, 9] = item.Category;
                            i++;
                        }

                        wb.SaveAs(saveFileDialog.FileName, XlFileFormat.xlWorkbookDefault);
                        app.Quit();
                        Thread.Sleep(1000);
                    }).ContinueWith((task) =>
                    {
                        ProgressIndicator.IsBusy = false;


                        if (File.Exists(saveFileDialog.FileName))
                        {
                            MessageBox.Show("Your data has been successfully exported.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Fail to export to Excel.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }, TaskScheduler.FromCurrentSynchronizationContext());

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Internal Error while export to Excel: " + ex.Message);
            }
        }

        private void lvSuppliers_Click(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;

            if (headerClicked != null)
            {
                if (headerClicked == _lastHeaderClicked)
                {
                    if (_lastDirection == ListSortDirection.Ascending)
                        _lastDirection = ListSortDirection.Descending;
                    else
                        _lastDirection = ListSortDirection.Ascending;
                }
                else
                {
                    _lastDirection = ListSortDirection.Ascending;
                }

                switch (headerClicked.Content)
                {
                    case "ID":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in supplierList
                                         orderby p.Id
                                         select p).ToList();
                            supplierList = pList;
                        }
                        else
                        {
                            var pList = (from p in supplierList
                                         orderby p.Id descending
                                         select p).ToList();
                            supplierList = pList;
                        }

                        break;
                    case "NAME":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in supplierList
                                         orderby p.Name
                                         select p).ToList();
                            supplierList = pList;
                        }
                        else
                        {
                            var pList = (from p in supplierList
                                         orderby p.Name descending
                                         select p).ToList();
                            supplierList = pList;
                        }
                        break;
                    case "Province":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in supplierList
                                         orderby p.Province
                                         select p).ToList();
                            supplierList = pList;
                        }
                        else
                        {
                            var pList = (from p in supplierList
                                         orderby p.Province descending
                                         select p).ToList();
                            supplierList = pList;
                        }
                        break;

                    case "CATEGORY":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in supplierList
                                         orderby p.Category
                                         select p).ToList();
                            supplierList = pList;
                        }
                        else
                        {
                            var pList = (from p in supplierList
                                         orderby p.Category descending
                                         select p).ToList();
                            supplierList = pList;
                        }
                        break;
                }

                lvSuppliers.ItemsSource = supplierList;
                _lastHeaderClicked = headerClicked;
            }
        }
    }
}
