﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class Supplier
    {

        private int _id;
        private string _name;
        private string _address1;
        private string _address2;
        private string _province;
        private string _postalCode;
        private string _phone;
        private string _email;
        private string _category;

        public Supplier(int id, string name, string address1, string address2, string province, string postalCode, string phone, string email, string category)
        {
            this.Id = id;
            this.Name = name;
            this.Address1 = address1;
            this.Address2 = address2;
            this.Province = province;
            this.PostalCode = postalCode;
            this.Phone = phone;
            this.Email = email;
            this.Category = category;

        }

        public Supplier()
        {

        }


        public int Id
        {
            get
            {
                return _id;
            }
            set
            {

                _id = value;
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z ]{2,50}$").Success)

                {
                    throw new InvalidDataException("Name must be between 2 and 50 characters");
                }
                _name = value;
            }
        }
        public string Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z0-9, ]{1,150}$").Success)

                {
                    throw new InvalidDataException("Address1 must be between 1 and 150 characters");
                }
                _address1 = value;
            }
        }
        public string Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z0-9, ]{1,150}$").Success)

                {
                    throw new InvalidDataException("Address2 must be between 1 and 150 characters");
                }
                _address2 = value;
            }
        }

        public string Province
        {
            get
            {
                return _province;
            }
            set
            {

                _province = value;
            }
        }

        public string PostalCode
        {
            get
            {
                return _postalCode;
            }
            set
            {
                if (!Regex.Match(value, "[ABCEGHJKLMNPRSTVXY][0-9][A-Z] [0-9][A-Z][0-9]").Success)

                {
                    throw new InvalidDataException("This is not a valid postal Code");
                }
                _postalCode = value;
            }
        }

        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                if (!Regex.Match(value, "^((([0-9]{1})*[- .(]*([0-9]{3})[- .)]*[0-9]{3}[- .]*[0-9]{4})+)*$").Success)

                {
                    throw new InvalidDataException("Please enter valid phone numbers");
                }
                _phone = value;
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$").Success)

                {
                    throw new InvalidDataException("Please enter valid email address");
                }
                _email = value;
            }
        }
        public string Category
        {
            get
            {
                return _category;
            }
            set
            {

                _category = value;
            }
        }

        
        public override string ToString()
        {
            return _name;
        }
    }
}
