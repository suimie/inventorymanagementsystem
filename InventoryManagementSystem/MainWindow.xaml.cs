﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<TabItem> _tabItems = new List<TabItem>();
//        private TabItem _tabAdd = null;
        private List<Product> reminderList;
        List<Product> productList;

        private ManageSuppliersControl supplierMtmtControl;
        private ManageProductsControl productMgmtControl;
        private PreferenceChartControl preferChart;
        private ManageSalesControl salesMgmtControl;
        public MainWindow()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + "../.."));
            InitializeComponent();
            Show();

            Utility.mainWindow = this;
            MenuItem_Login(null, null);
        }

        public void RefreshReminders()
        {
            ProgressIndicator.IsBusy = true;

            Task.Factory.StartNew(() =>
            {
                reminderList = Database.getInstance().GetProductsCloseToLimit();
                Thread.Sleep(1000);
            }).ContinueWith((task) =>
            {
                lvReminders.ItemsSource = reminderList;
                lvReminders.Items.Refresh();
                ProgressIndicator.IsBusy = false;
            }, TaskScheduler.FromCurrentSynchronizationContext());

        }

        private int IsOpenAlready(string menuName)
        {
            int nth = 0;
            foreach (TabItem item in _tabItems)
            {
                if (item.Name == menuName) return nth;
                nth++;
            }

            return nth;
        }

        private TabItem AddTabItem(String menuName)
        {
            int count = _tabItems.Count;
            int nth = IsOpenAlready(menuName);
            if (nth  < count)
            {
                tabDynamic.SelectedIndex = nth;
                return null;
            }

            // create new tab item
            TabItem tab = new TabItem();
            //tab.Header = string.Format("Tab {0}", count);
            //tab.Name = string.Format("tab{0}", count);
            //ManageProducts
            tab.Header = menuName;
            tab.Name = menuName;
            tab.HeaderTemplate = tabDynamic.FindResource("TabHeader") as DataTemplate;

            // add controls to tab item
            Grid grid = new Grid();
            switch (menuName)
            {
                case "ManageSuppliers":
                    supplierMtmtControl = new ManageSuppliersControl();
                    grid.Children.Add(supplierMtmtControl);
                    break;
                case "ManageProducts":
                    productMgmtControl = new ManageProductsControl();
                    grid.Children.Add(productMgmtControl);
                    break;
                case "SalesChart":
                    preferChart = new PreferenceChartControl();
                    grid.Children.Add(preferChart);
                    break;
                case "ManageSales":
                    salesMgmtControl = new ManageSalesControl();
                    grid.Children.Add(salesMgmtControl);
                    break;
            }
            grid.Background = Brushes.Red;
            grid.VerticalAlignment = VerticalAlignment.Stretch;
            grid.HorizontalAlignment = HorizontalAlignment.Stretch;
            tab.Content = grid;

            tabDynamic.DataContext = null;
            _tabItems.Insert(count, tab);

            // bind tab control
            tabDynamic.DataContext = _tabItems;
            tabDynamic.SelectedIndex = count;
            return tab;
        }

        private void tabDynamic_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabItem tab = tabDynamic.SelectedItem as TabItem;

            if (tab != null && tab.Header != null)
            {
                switch (tab.Header)
                {
                    case "ManageSuppliers":
                        //supplierMtmtControl;
                        break;
                    case "ManageProducts":
                        //productMgmtControl;
                        break;
                    case "SalesChart":
                        preferChart.btSeeTop20_Click(null, null);
                        break;
                    case "ManageSales":
                        //salesMgmtControl;
                        break;
                }
                /*
                if (tab.Header.Equals(_tabAdd))
                {
                    // clear tab control binding
                    tabDynamic.DataContext = null;

                    // add new tab
                    TabItem newTab = this.AddTabItem(tab.Name);

                    // bind tab control
                    tabDynamic.DataContext = _tabItems;

                    // select newly added tab item
                    tabDynamic.SelectedItem = newTab;
                }*/
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            string tabName = (sender as Button).CommandParameter.ToString();

            var item = tabDynamic.Items.Cast<TabItem>().Where(i => i.Name.Equals(tabName)).SingleOrDefault();

            TabItem tab = item as TabItem;

            if (tab != null)
            {
                if (MessageBox.Show(string.Format("Are you sure you want to remove the tab '{0}'?", tab.Header.ToString()),
                    "Remove Tab", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    // get selected tab
                    TabItem selectedTab = tabDynamic.SelectedItem as TabItem;

                    // clear tab control binding
                    tabDynamic.DataContext = null;

                    _tabItems.Remove(tab);

                    // bind tab control
                    tabDynamic.DataContext = _tabItems;

                    // select previously selected tab. if that is removed then select first tab
                    if (selectedTab == null || selectedTab.Equals(tab))
                    {
                        if (_tabItems.Count == 0)
                            selectedTab = null;
                        else
                            selectedTab = _tabItems[0];
                    }
                    if(selectedTab != null)
                        tabDynamic.SelectedItem = selectedTab;
                }
            }
        }


        private void MenuItem_ManageSuppliers(object sender, RoutedEventArgs e)
        {
            /*
            ManageSuppliers mSuppliers = new ManageSuppliers();
            mSuppliers.ShowDialog();
            */
            AddTabItem("ManageSuppliers");

        }

        private void MenuItem_ManageProducts(object sender, RoutedEventArgs e)
        {
            AddTabItem("ManageProducts");
        }

        private void MenuItem_ManageSales(object sender, RoutedEventArgs e)
        {
            /*
           ManageSales mSales = new ManageSales();
           mSales.ShowDialog();
           */
            AddTabItem("ManageSales");
        }


        private void MenuItem_Login(object sender, RoutedEventArgs e)
        {
            Login loginDialog = new Login();

            if(loginDialog.ShowDialog() == true)
            {
                miFile.IsEnabled = true;
                miManage.IsEnabled = true;
                btSupplierMgmt.IsEnabled = true;
                btGoOrder.IsEnabled = true;
                btProductMgmt.IsEnabled = true;
                btSalesChart.IsEnabled = true;
                btSalesMgmt.IsEnabled = true;
                RefreshReminders();
            }
            else
            {
                miFile.IsEnabled = true;
                miManage.IsEnabled = false;
                btSupplierMgmt.IsEnabled = false;
                btGoOrder.IsEnabled = false;
                btProductMgmt.IsEnabled = false;
                btSalesChart.IsEnabled = false;
                btSalesMgmt.IsEnabled = false;
            }
        }

        private void MenuItem_Logout(object sender, RoutedEventArgs e)
        {
            if(_tabItems.Count > 0)
            {
                MessageBox.Show("Close the tabs first then logout!", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            lvReminders.ItemsSource = null;
            lvReminders.Items.Refresh();

            miFile.IsEnabled = true;
            miManage.IsEnabled = false;
            btSupplierMgmt.IsEnabled = false;
            btGoOrder.IsEnabled = false;
            btProductMgmt.IsEnabled = false;
            btSalesChart.IsEnabled = false;
            btSalesMgmt.IsEnabled = false;
        }

        private void btSalesChart_Click(object sender, RoutedEventArgs e)
        {
            AddTabItem("SalesChart");
        }

        private void btSalesMgmt_Click(object sender, RoutedEventArgs e)
        {
            AddTabItem("ManageSales");
        }


        private void miSendEmial_Click(object sender, RoutedEventArgs e)
        {
            int currProductIdx = lvReminders.SelectedIndex;
            productList = Database.getInstance().GetAllProducts();
            int supplierId = productList[currProductIdx].SupplierId;
            int productId = productList[currProductIdx].Id;


            Supplier supplier = Database.getInstance().GetSupplierById(supplierId);
            Product product = Database.getInstance().GetProductbyId(productId);

            String msg = String.Format("Do you want to send an email to {0} ordering product [{1}-{2}]", supplier.Email, product.Id, product.Name);
            if (MessageBox.Show(msg, "Request Purchase", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                //string emailContent = 
                var url = "mailto:" + supplier.Email + "?subject=Request Purchase&body=" + "Hello, I would like to order product Id:" + product.Id + ", Name : " + product.Name;
                System.Diagnostics.Process.Start(url);
            }

        }

        private void btGoOrder_Click(object sender, RoutedEventArgs e)
        {
            AddTabItem("ManageProducts");
        }

        private void miChart_Click(object sender, RoutedEventArgs e)
        {
            AddTabItem("SalesChart");
        }

        private void tabDynamic_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
