﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Data;

namespace InventoryManagementSystem
{
    class Database
    {
        private MySqlConnection conn;
        private static Database instance;
        private string hostName = "den1.mysql4.gear.host";
        private string databaseName = "valinidatabase";
        private string userName = "valinidatabase";
        private string password = "Valiniipd12!";


        public Database()
        {
            string connectString = string.Format("SERVER ={0};DATABASE={1};UID={2};PWD={3};", hostName, databaseName, userName, password);
            Console.WriteLine(connectString);
            conn = new MySqlConnection(connectString);
            
            conn.Open();
        }

        public static Database getInstance()
        {
            if (instance == null)
            {
                instance = new Database();
            }

            return instance;
        }


        public List<Supplier> GetAllSuppliers()
        {

            List<Supplier> supplierList = new List<Supplier>();
            using (MySqlCommand command = new MySqlCommand("SELECT * FROM Suppliers", conn))
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["suppName"];
                    string address1 = (string)reader["suppAddress1"];
                    string address2 = (string)reader["suppAddress2"];
                    string province = (string)reader["suppProvince"];
                    string postalCode = (string)reader["suppPostalCode"];
                    string phone = (string)reader["suppPhone"];
                    string email = (string)reader["suppEmail"];
                    string category = (string)reader["suppCategory"];
                    Supplier supplier = new Supplier(id, name, address1, address2, province, postalCode, phone, email, category);
                    
                    supplierList.Add(supplier);


                }
            }
            return supplierList;


        }
        public void AddSupplier(Supplier s)
        {
            string sql = "INSERT INTO Suppliers (suppName, suppAddress1, suppAddress2, suppProvince, suppPostalCode, suppPhone, suppEmail, suppCategory) VALUES (@suppName,@suppAddress1, @suppAddress2, @suppProvince, @suppPostalCode, @suppPhone, @suppEmail, @suppCategory); ";
             
            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@suppName", s.Name);
                cmd.Parameters.AddWithValue("@suppAddress1", s.Address1);
                cmd.Parameters.AddWithValue("@suppAddress2", s.Address2);
                cmd.Parameters.AddWithValue("@suppProvince", s.Province);
                cmd.Parameters.AddWithValue("@suppPostalCode", s.PostalCode);
                cmd.Parameters.AddWithValue("@suppPhone", s.Phone);
                cmd.Parameters.AddWithValue("@suppEmail", s.Email);
                cmd.Parameters.AddWithValue("@suppCategory", s.Category);
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteSupplierById(int id)
        {

            string sql = "DELETE FROM Suppliers WHERE Id=@Id";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", MySqlDbType.Int32).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();


        }

        public Supplier GetSupplierById(int id)
        {

            Supplier supplier = new Supplier();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Suppliers WHERE id=@id", conn);
            command.Parameters.AddWithValue("@id", id);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    supplier.Id = id;
                    supplier.Name = (string)reader["suppName"];
                    supplier.Address1 = (string)reader["suppAddress1"];
                    supplier.Address2 = (string)reader["suppAddress2"];
                    supplier.Province = (string)reader["suppProvince"];
                    supplier.PostalCode = (string)reader["suppPostalCode"];
                    supplier.Phone = (string)reader["suppPhone"];
                    supplier.Email = (string)reader["suppEmail"];
                    supplier.Category = (string)reader["suppCategory"];
                }
            }

            return supplier;
        }


        public List<Supplier> GetSupplierByCategory(string category)
        {
            List<Supplier> supplierList = new List<Supplier>();

            MySqlCommand command = null;
            if(category == "All")
            {
                command = new MySqlCommand("SELECT * FROM Suppliers", conn);
            }
            else
            {
                command = new MySqlCommand("SELECT * FROM Suppliers WHERE suppCategory=@Category", conn);
                command.Parameters.AddWithValue("@Category", category);
            }

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Supplier supplier = new Supplier();

                    supplier.Id = (int)reader["Id"];
                    supplier.Name = (string)reader["suppName"];
                    supplier.Address1 = (string)reader["suppAddress1"];
                    supplier.Address2 = (string)reader["suppAddress2"];
                    supplier.Province = (string)reader["suppProvince"];
                    supplier.PostalCode = (string)reader["suppPostalCode"];
                    supplier.Phone = (string)reader["suppPhone"];
                    supplier.Email = (string)reader["suppEmail"];
                    supplier.Category = (string)reader["suppCategory"];

                    supplierList.Add(supplier);
                }
            }

            return supplierList;
        }


        public List<Product> GetAllProducts()
        {
            List<Product> productList = new List<Product>();
            string sql = "SELECT p.id PId, s.id supplierId, s.suppName spplierName, s.suppCategory Category, " +
                " p.productName productName, p.description description, p.price price, p.inventoryOnHand inventoryOnHand, " +
                " p.inventoryReceived inventoryReceived, p.inventorysold inventorysold, p.minInventoryLevel minInventoryLevel " +
                " FROM products p JOIN suppliers s ON p.supplierId = s.id; ";
            using (MySqlCommand command = new MySqlCommand(sql, conn))
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["PId"];
                    int sid = (int)reader["supplierId"];
                    string sname = (string)reader["spplierName"];
                    string category = (string)reader["Category"];
                    string name = (string)reader["productName"];
                    string description = (string)reader["description"];
                    decimal price = (decimal)reader["price"];
                    int onHand = (int)reader["inventoryOnHand"];
                    int received = (int)reader["inventoryReceived"];
                    int sold = (int)reader["inventorySold"];
                    int minLevel = (int)reader["minInventoryLevel"];

                    Product product = new Product(id, sid, sname, category, name, description, price, received, sold, onHand, minLevel);
                    productList.Add(product);
                }
            }
            return productList;


        }

        public Product GetProductbyId(int id)
        {
           Product product =null;
            string sql = "SELECT  s.id supplierId, s.suppName spplierName, s.suppCategory Category, " +
                " p.productName productName, p.description description, p.price price, p.inventoryOnHand inventoryOnHand, " +
                " p.inventoryReceived inventoryReceived, p.inventorysold inventorysold, p.minInventoryLevel minInventoryLevel " +
                " FROM products p JOIN suppliers s ON p.supplierId = s.id WHERE p.id=@Id ";
            MySqlCommand command = new MySqlCommand(sql, conn);
                command.Parameters.AddWithValue("@Id", id);
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    
                    int sid = (int)reader["supplierId"];
                    string sname = (string)reader["spplierName"];
                    string category = (string)reader["Category"];
                    string name = (string)reader["productName"];
                    string description = (string)reader["description"];
                    decimal price = (decimal)reader["price"];
                    int onHand = (int)reader["inventoryOnHand"];
                    int received = (int)reader["inventoryReceived"];
                    int sold = (int)reader["inventorySold"];
                    int minLevel = (int)reader["minInventoryLevel"];

                    product = new Product(id, sid, sname, category, name, description, price, received, sold, onHand, minLevel);

                }
            }
            return product;


        }
        public Product GetProductbyName(string name)
        {
            Product product = null;
            string sql = "SELECT  p.id PId, s.id supplierId, s.suppName spplierName, s.suppCategory Category, " +
                " p.productName productName, p.description description, p.price price, p.inventoryOnHand inventoryOnHand, " +
                " p.inventoryReceived inventoryReceived, p.inventorysold inventorysold, p.minInventoryLevel minInventoryLevel " +
                " FROM products p JOIN suppliers s ON p.supplierId = s.id WHERE p.productName=@Name ";
            MySqlCommand command = new MySqlCommand(sql, conn);
            command.Parameters.AddWithValue("@Name", name);
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["PId"];
                    int sid = (int)reader["supplierId"];
                    string sname = (string)reader["spplierName"];
                    string category = (string)reader["Category"];
                    //string name = (string)reader["productName"];
                    string description = (string)reader["description"];
                    decimal price = (decimal)reader["price"];
                    int onHand = (int)reader["inventoryOnHand"];
                    int received = (int)reader["inventoryReceived"];
                    int sold = (int)reader["inventorySold"];
                    int minLevel = (int)reader["minInventoryLevel"];

                    product = new Product(id, sid, sname, category, name, description, price, received, sold, onHand, minLevel);

                }
            }
            return product;


        }


        public List<Product> GetProductsCloseToLimit()
        {
            List<Product> plist = new List<Product>();

            string sql = "SELECT  Id, productName " +
                " FROM products  WHERE (inventoryOnHand - minInventoryLevel) <= 5 ";
            MySqlCommand command = new MySqlCommand(sql, conn);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["productName"];

                    Product product = new Product(id, name);
                    plist.Add(product);
                }
            }
            return plist;
        }

        public void AddProduct(Product p)
        {
            string sql = "INSERT INTO Products " +
                "(supplierId, productName, description, price, inventoryOnHand, inventoryReceived, inventorySold, minInventoryLevel) " +
                "VALUES (@supplierId, @productName,@description, @price, @inventoryOnHand, @inventoryReceived, @inventorySold, @minInventoryLevel); ";

            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@supplierId", p.SupplierId);
                cmd.Parameters.AddWithValue("@productName", p.Name);
                cmd.Parameters.AddWithValue("@description", p.Description);
                cmd.Parameters.AddWithValue("@price", p.Price);
                cmd.Parameters.AddWithValue("@inventoryOnHand", p.OnHand);
                cmd.Parameters.AddWithValue("@inventoryReceived", p.Received);
                cmd.Parameters.AddWithValue("@inventorySold", p.Sold);
                cmd.Parameters.AddWithValue("@minInventoryLevel", p.MinLevel);
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateProduct(Product p)
        {
            string sql = "UPDATE Products " +
                        " SET supplierId=@SupplierId, productName=@Name, description=@Description, price=@Price, minInventoryLevel=@MinLevel " +
                        " WHERE Id=@Id";
            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@supplierId", p.SupplierId);
                cmd.Parameters.AddWithValue("@Name", p.Name);
                cmd.Parameters.AddWithValue("@Description", p.Description);
                cmd.Parameters.AddWithValue("@Price", p.Price);
                cmd.Parameters.AddWithValue("@MinLevel", p.MinLevel);
                cmd.Parameters.AddWithValue("@Id", p.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteProductById(int id)
        {

            string sql = "DELETE FROM Products WHERE Id=@Id";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", MySqlDbType.Int32).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        public void UpdateSupplier(Supplier s)
        {
            string sql = "UPDATE Suppliers SET suppName = @suppName,  suppAddress1 = @suppAddress1, suppAddress2=@suppAddress2, suppProvince= @suppProvince, suppPostalCode=@suppPostalCode, suppPhone=@suppPhone, suppEmail=@suppEmail, suppCategory= @suppCategory WHERE Id=@Id";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Id", s.Id);
            cmd.Parameters.AddWithValue("@suppName", s.Name);
            cmd.Parameters.AddWithValue("@suppAddress1", s.Address1);
            cmd.Parameters.AddWithValue("@suppAddress2", s.Address2);
            cmd.Parameters.AddWithValue("@suppProvince", s.Province);
            cmd.Parameters.AddWithValue("@suppPostalCode", s.PostalCode);
            cmd.Parameters.AddWithValue("@suppPhone", s.Phone);
            cmd.Parameters.AddWithValue("@suppEmail", s.Email);
            cmd.Parameters.AddWithValue("@suppCategory", s.Category);
            cmd.ExecuteNonQuery();
        }

        public void AddPurchase(Purchase purchase)
        {
            string sql = "INSERT INTO Purchases " +
                "(supplierId, productId, numberReceived, purchaseDate) " +
                "VALUES (@supplierId, @productId,@received, @purchaseDate); ";

            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@supplierId", purchase.SupplierId);
                cmd.Parameters.AddWithValue("@productId", purchase.ProductId);
                cmd.Parameters.AddWithValue("@received", purchase.Received);
                cmd.Parameters.AddWithValue("@purchaseDate", purchase.PurchaseDate);

                cmd.ExecuteNonQuery();
            }
        }

        public List<SalesPreference> GetSalesListByPeriodNCategory(DateTime from, DateTime to, string category, bool isLimit)
        {
            List<SalesPreference> preferenceList = new List<SalesPreference>();

            string sql = "SELECT salesdetails.productId ProductId, p.productName ProductName, SUM(salesdetails.quantity) Quantity " +
                        "FROM salesdetails JOIN products p ON salesdetails.productId = p.id JOIN suppliers s ON p.supplierId = s.id ";
            if (category != "All")
            {
                sql += "WHERE s.suppCategory = @Category AND " + 
                    "salesdetails.salesId IN(SELECT id FROM sales WHERE salesDate >= @From AND salesDate <= @To ) ";
            }
            else
            {
                sql += "WHERE salesdetails.salesId IN(SELECT id FROM sales WHERE salesDate >= @From AND salesDate <= @To ) ";
            }

            sql += "GROUP BY salesdetails.productId ";
            if (isLimit)
                sql += "ORDER BY Quantity DESC LIMIT 20; ";




            MySqlCommand command = new MySqlCommand(sql, conn);
            command.Parameters.AddWithValue("@From", from);
            command.Parameters.AddWithValue("@To", to);
            command.Parameters.AddWithValue("@Category", category);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                    int pid = (int)reader["ProductId"];
                    string pname = (string)reader["ProductName"];
                    Decimal sumOfSold = (Decimal)reader["Quantity"];

                    SalesPreference p = new SalesPreference();
                    p.ProductId = pid;
                    p.ProductName = pname;
                    p.SumOfSold = sumOfSold;

                    preferenceList.Add(p);
                }
            }
            
            return preferenceList;
        }

        public Decimal GetMaxOfByPeriodNCategory(DateTime from, DateTime to, string category)
        {
            List<SalesPreference> preferenceList = new List<SalesPreference>();

            string sql = "SELECT SUM(salesdetails.quantity) MaxQuantity " +
                        "FROM salesdetails JOIN products p ON salesdetails.productId = p.id JOIN suppliers s ON p.supplierId = s.id ";
            if (category != "All")
            {
                sql += "WHERE s.suppCategory = @Category AND " +
                    "salesdetails.salesId IN(SELECT id FROM sales WHERE salesDate >= @From AND salesDate <= @To ) ";
            }
            else
            {
                sql += "WHERE salesdetails.salesId IN(SELECT id FROM sales WHERE salesDate >= @From AND salesDate <= @To ) ";
            }

            sql += "GROUP BY salesdetails.productId " +
                        "ORDER BY Quantity DESC LIMIT 1; ";


            MySqlCommand command = new MySqlCommand(sql, conn);
            command.Parameters.AddWithValue("@From", from);
            command.Parameters.AddWithValue("@To", to);
            command.Parameters.AddWithValue("@Category", category);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Decimal sum = (Decimal)reader["MaxQuantity"];
                    return sum;
                }
            }

            return Decimal.Zero;
        }

        public int AddSales(DateTime salesDate, double subTot, double tps, double tvq, double total)
        {
            string sql = "INSERT INTO Sales " +
                        "(salesDate, subTotal, tps, tvq, total) " +
                        "VALUES (@SalesDate, @SubTotal, @TPS, @TVQ, @Total); ";
                        //+"SELECT CAST(scope_identity() AS int)";

            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@SalesDate", salesDate);
                cmd.Parameters.AddWithValue("@SubTotal", subTot);
                cmd.Parameters.AddWithValue("@TPS", tps);
                cmd.Parameters.AddWithValue("@TVQ", tvq);
                cmd.Parameters.AddWithValue("@Total", total);

                //int id = (int)cmd.ExecuteScalar();
                cmd.ExecuteNonQuery();
                int id = (int)cmd.LastInsertedId;

                return id;
            }
        }

        public void AddSalesDetailLine(int sid, Sales sales)
        {
            string sql = "INSERT INTO Salesdetails " +
                        "( salesId, productId, quantity) " +
                        "VALUES (@SalesId, @ProductId, @Quantity); ";

            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@SalesId", sid);
                cmd.Parameters.AddWithValue("@ProductId", sales.ProductId);
                cmd.Parameters.AddWithValue("@Quantity", sales.QuantitySold);
                cmd.ExecuteNonQuery();
            }
        }


        public int GetLastIdInserted() {
            string sql = "SELECT LAST_INSERT_ID(); ";
            MySqlCommand command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
            long idL = command.LastInsertedId;
            int id = Convert.ToInt32(idL);
            return id;

        }
        
        public void closeConn()
        {
            conn.Close();
            conn.Dispose();
        }
    }
}
