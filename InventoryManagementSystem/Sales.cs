﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class Sales
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public DateTime SalesDate { get; set; }
        public decimal Price { get; set; }
        public int QuantitySold { get; set; }
        public decimal Amount { get; set; }


        public Sales(int id, int productId, string productName, DateTime salesDate,decimal price, int quantitySold )
        {
            Id = id;
            ProductId = productId;
            ProductName = productName;
            SalesDate = salesDate;
            Price = price;
            QuantitySold = quantitySold;
            
        }

        public Sales(int id, int productId, string productName, DateTime salesDate, decimal price, int quantitySold, decimal amount)
        {
            Id = id;
            ProductId = productId;
            ProductName = productName;
            SalesDate = salesDate;
            Price = price;
            QuantitySold = quantitySold;
            Amount = amount;

        }

        public Sales(int id, string productName, int quantitySold, decimal price, decimal amount)
        {
            Id = id;
            ProductName = productName;
            QuantitySold = quantitySold;
            Price = price;
            Amount = amount;


        }

        public Sales(int id, int productId, DateTime salesDate, int quantitySold) {
            Id = id;
            ProductId = productId;
            SalesDate = salesDate;
            QuantitySold = quantitySold;


        }


        public Sales()
        {
           
        }
    }


}

