﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class Product
    {
        public Product()
        {

        }

        public Product(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public Product(int id, int sid, string sName, string category, string name, string description, decimal price, int quantity, int minLevel)
        {
            Id = id;
            SupplierId = sid;
            SupplierName = sName;
            Category = category;
            Name = name;
            Description = description;
            Price = price;
            Received = quantity;
            Sold = 0;
            OnHand = Received - Sold;
            MinLevel = minLevel;
        }

        public Product(int id, int sid, string sName, string category, string name, string description, decimal price, int received, int sold, int onHand, int minLevel)
        {
            Id = id;
            SupplierId = sid;
            SupplierName = sName;
            Category = category;
            Name = name;
            Description = description;
            Price = price;
            Received = received;
            Sold = sold;
            OnHand = onHand;
            MinLevel = minLevel;
        }

        public int Id { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int OnHand { get; set; }
        public int Received { get; set; }
        public int Sold { get; set; }
        public int MinLevel { get; set; }

        public bool IsCloseToLimit
        {
            get
            {
                if (MinLevel + 5 >= OnHand)
                    return true;
                else
                    return false;
            }
        }
    }
}
