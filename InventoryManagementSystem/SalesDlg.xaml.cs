﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for SalesDlg.xaml
    /// </summary>
    ///
     
    public partial class SalesDlg : Window
    {
        Product currProduct;
        List<Sales> currSalesList;
        public SalesDlg(Product product, List<Sales> salesList)
        {
            currProduct = product;
            currSalesList = salesList;

            InitializeComponent();

            tbQuantity.Focus();

            tbCode.IsEnabled = false;
            tbName.IsEnabled = false;
            tbDesp.IsEnabled = false;
            tbSupplier.IsEnabled = false;
            tbCategory.IsEnabled = false;
            tbPrice.IsEnabled = false;
            tbOnHand.IsEnabled = false;
           

            tbCode.Text = product.Id + "";
            tbName.Text = product.Name;
            tbDesp.Text = product.Description;
            tbSupplier.Text = product.SupplierName;
            tbCategory.Text = product.Category;
            tbPrice.Text = product.Price.ToString();
            tbOnHand.Text = product.OnHand + "";
            dpSalesDate.SelectedDate = DateTime.Now;

            Dictionary<int, int> onHandAfterCheckLV = new Dictionary<int, int>();
            foreach(Sales s in salesList)
            {
                if(s.ProductId == currProduct.Id)
                {
                    if(!onHandAfterCheckLV.ContainsKey(currProduct.Id))
                    {
                        onHandAfterCheckLV.Add(currProduct.Id, product.OnHand - s.QuantitySold);
                    }
                    else
                    {
                        onHandAfterCheckLV[currProduct.Id] -= s.QuantitySold;
                    }
                    tbOnHand.Text = onHandAfterCheckLV[currProduct.Id] + "";
                }
            }

            if(onHandAfterCheckLV.ContainsKey(currProduct.Id) && onHandAfterCheckLV[currProduct.Id] - 5 <= currProduct.MinLevel)
            {
                MessageBox.Show("The Inventory On Hand is close to Minimum level of Inventory.  Reorder soon for the product[" + currProduct.Name + "].",
                    "Message", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            tbQuantity.Text = "";
        }

        private void btSales_Click(object sender, RoutedEventArgs e)
        {
            if (tbQuantity.Text == "") return;
            int onHand= int.Parse(tbOnHand.Text);
            if ((int.TryParse(tbQuantity.Text, out int quantitySold)) && (quantitySold <= onHand))
            {
                DateTime salesDate = (DateTime)dpSalesDate.SelectedDate;
                int productId = currProduct.Id;
                string productName= currProduct.Name;
                decimal price = currProduct.Price;
                decimal amount = (decimal)(quantitySold * price);
                Sales sales = new Sales(ManageSalesControl.ordering++, productId, productName, salesDate, price, quantitySold, amount);
                //Sales sales = new Sales(0, productId, salesDate, quantitySold);
                currSalesList.Add(sales);
               //Sales salesLvList = new Sales(0, productName, quantitySold, price,amount );
               //salesList.Add(salesLvList);
//              int salesId=Database.getInstance().AddSalesId(sales);
             //  Database.getInstance().GetSalesbyId(salesId);

                DialogResult = true;

            }

            else {

                MessageBox.Show("Invalid Quantity.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;



            }
            


        }
    }
}
