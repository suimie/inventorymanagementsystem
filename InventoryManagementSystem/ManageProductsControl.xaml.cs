﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InventoryManagementSystem
{
    /// <summary>
    /// Interaction logic for ManageProductsControl.xaml
    /// </summary>
    public partial class ManageProductsControl : UserControl
    {
        int currProductIdx = -1;
        List<Supplier> supplierList;
        List<Product> productList;
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        public ManageProductsControl()
        {
            InitializeComponent();
            Canvas.SetLeft(this, 210);
            Canvas.SetTop(this, 50);

            tbCode.IsEnabled = false;
            tbCategory.IsEnabled = false;

            try
            {
                InitializeFields();

                refreshProductsList();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error selecting database: " + e.Message);
            }
        }

        private void InitializeFields()
        {
            btClear_Click(null, null);
            //cmbCategory.SelectedIndex = 0;
        }

        private void refreshProductsList()
        {
            try
            {
                ProgressIndicator.IsBusy = true;

                Task.Factory.StartNew(() =>
                {
                    productList = Database.getInstance().GetAllProducts();
                }).ContinueWith((task) =>
                {
                    lvProduct.ItemsSource = productList;
                    tbSearch.Text = "";
                    ProgressIndicator.IsBusy = false;

                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error selecting database: " + e.Message);
            }
        }

        private void cmbSupplier_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbSupplier.SelectedIndex < 0) {
                return;
            }

            tbCategory.Text = supplierList[cmbSupplier.SelectedIndex].Category;
        }

        private void SetFieldsWithSelectedProduct()
        {
            if (currProductIdx < 0) return;

            Product product = (Product)lvProduct.SelectedValue;

            tbCode.Text = product.Id + "";
            tbName.Text = product.Name;
            tbDesp.Text = product.Description;
            //tbQuantity.Text = product.OnHand.ToString();
            tbPrice.Text = product.Price.ToString();
            tbMinLevel.Text = product.MinLevel.ToString();

            int supplierIdx = FindSupplierIdx(product.SupplierId);
            if (supplierIdx >= 0)
                cmbSupplier.SelectedIndex = supplierIdx;
        }

        private int FindSupplierIdx(int id)
        {
            int count = 0;
            foreach (Supplier s in supplierList)
            {
                if (s.Id == id) return count;

                count++;
            }

            return -1;
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            tbCode.Text = "...";
            tbName.Text = "";
            tbDesp.Text = "";
            //tbQuantity.Text = "";
            tbPrice.Text = "";
            tbMinLevel.Text = "";

            cmbCategory.SelectedIndex = 0;
            cmbSupplier.SelectedIndex = 0;

            btSave.Content = "Add New Product";
            //tbQuantity.IsEnabled = true;
            cmbSupplier.IsEnabled = true;
            cmbCategory.IsEnabled = true;

            lvProduct.SelectedIndex = -1;
            currProductIdx = -1;
            cmbCategory.SelectedIndex = 0;

            tbName.Focus();
        }


        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;

            if (name == "")
            {
                MessageBox.Show("Invalid Product Name.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            string description = tbDesp.Text;


            if (!decimal.TryParse(tbPrice.Text, out decimal price))
            {
                MessageBox.Show("Invalid Price.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (!int.TryParse(tbMinLevel.Text, out int minLevel))
            {
                MessageBox.Show("Invalid Minimum Levels.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            try
            {
                int supplierIdx = cmbSupplier.SelectedIndex;

                Product product = (currProductIdx < 0) ?
                    new Product(0, supplierList[supplierIdx].Id, supplierList[supplierIdx].Name, supplierList[supplierIdx].Category, name, description, price, 0, minLevel) :
                    (Product)lvProduct.SelectedValue;
                    //productList[currProductIdx];
                if (currProductIdx >= 0)
                {
                    product.Name = name;
                    product.Description = description;
                    product.SupplierId = supplierList[supplierIdx].Id;
                    product.Price = price;
                    product.MinLevel = minLevel;

                    Database.getInstance().UpdateProduct(product);
                }
                else
                {
                    Database.getInstance().AddProduct(product);
                }


                refreshProductsList();

                btClear_Click(null, null);
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Error updating database: " + ex.Message);
            }

        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvProduct.SelectedIndex < 0)
            {
                btClear_Click(null, null);
                return;
            }
            


            currProductIdx = lvProduct.SelectedIndex;
            SetFieldsWithSelectedProduct();

            cmbSupplier.IsEnabled = false;
            cmbCategory.IsEnabled = false;
            btSave.Content = "Update";
        }

        private void btPurchase_Click(object sender, RoutedEventArgs e)
        {
            if (currProductIdx < 0) return;

            Product product = (Product)lvProduct.SelectedValue;

            PurchaseWindow purchaseDialog = new PurchaseWindow(product);

            if (purchaseDialog.ShowDialog() == true)
            {
                refreshProductsList();

                if (ProgressIndicator.IsBusy)
                {
                    Thread.Sleep(1000);
                }

                Utility.mainWindow.RefreshReminders();
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Product> pList = productList;
            String word = tbSearch.Text;
            if (word != "")
            {
                var result = from p in pList
                             where p.Name.ToLower().Contains(word.ToLower()) ||
                             p.Description.ToLower().Contains(word.ToLower()) ||
                             p.SupplierName.ToLower().Contains(word.ToLower()) ||
                             p.Category.ToLower().Contains(word.ToLower())
                             select p;
                pList = result.ToList();
            }
            lvProduct.ItemsSource = pList;
        }


        private void btRequestPurchase_Click(object sender, RoutedEventArgs e)
        {
            if (currProductIdx < 0) return;

            try
            {
                Product product = (Product)lvProduct.SelectedValue;

                Supplier supplier = Database.getInstance().GetSupplierById(product.SupplierId);

                String msg = String.Format("Do you want to send an email to {0} ordering product [{1}-{2}]", supplier.Email, product.Id, product.Name);
                if (MessageBox.Show(msg, "Request Purchase", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    //string emailContent = 
                    var url = "mailto:" + supplier.Email + "?subject=Request Purchase&body=" + "Hello, I would like to order product Id:" + product.Id + ", Name : " + product.Name;
                    System.Diagnostics.Process.Start(url);
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Error selecting database: " + ex.Message);
            }

        }

        private void btToExcel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel Workbook|*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    ProgressIndicator.IsBusy = true;

                    Task.Factory.StartNew(() =>
                    {
                        Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                        Workbook wb = app.Workbooks.Add(XlSheetType.xlWorksheet);
                        Worksheet ws = (Worksheet)app.ActiveSheet;
                        app.Visible = false;

                        ws.Cells[1, 1] = "Code";
                        ws.Cells[1, 2] = "Product Name";
                        ws.Cells[1, 3] = "Received";
                        ws.Cells[1, 4] = "Sold";
                        ws.Cells[1, 5] = "On Hand";
                        ws.Cells[1, 6] = "MinLevel";
                        ws.Cells[1, 7] = "Price";
                        ws.Cells[1, 8] = "Supplier Name";
                        ws.Cells[1, 9] = "Category";


                        int i = 2;
                        foreach (Product item in productList)
                        {
                            ws.Cells[i, 1] = item.Id;
                            ws.Cells[i, 2] = item.Name;
                            ws.Cells[i, 3] = item.Received;
                            ws.Cells[i, 4] = item.Sold;
                            ws.Cells[i, 5] = item.OnHand;
                            ws.Cells[i, 6] = item.MinLevel;
                            ws.Cells[i, 7] = item.Price;
                            ws.Cells[i, 8] = item.SupplierName;
                            ws.Cells[i, 9] = item.Category;
                            i++;
                        }

                        wb.SaveAs(saveFileDialog.FileName, XlFileFormat.xlWorkbookDefault);
                        app.Quit();
                    }).ContinueWith((task) =>
                    {
                        ProgressIndicator.IsBusy = false;


                        if (File.Exists(saveFileDialog.FileName))
                        {
                            MessageBox.Show("Your data has been successfully exported.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Fail to export to Excel.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }, TaskScheduler.FromCurrentSynchronizationContext());

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Internal Error while export to Excel: " + ex.Message);
            }
        }

        private void cmbCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbCategory.SelectedIndex < 0)
            {
                return;
            }

            try
            {
                if (ProgressIndicator.IsBusy)
                {
                    Thread.Sleep(1000);
                }

                Utility.CATEGORY c = (Utility.CATEGORY)cmbCategory.SelectedIndex;
                string category = Enum.GetName(c.GetType(), c);//((ComboBoxItem)cmbCategory.SelectedItem).Content.ToString();
                supplierList = Database.getInstance().GetSupplierByCategory(category);

                cmbSupplier.ItemsSource = supplierList;
                cmbSupplier.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Internal Error: " + ex.Message);
            }
        }


        private void miSendEmail_Click(object sender, RoutedEventArgs e)
        {
            btRequestPurchase_Click(null, null);
        }

        private void miEnterPurchase_Click(object sender, RoutedEventArgs e)
        {
            btPurchase_Click(null, null);
        }

        // Sort list depending on which header is clicked
        private void lvProduct_ClickHeader(object sender, RoutedEventArgs e)
        {

            var headerClicked = e.OriginalSource as GridViewColumnHeader;

            if(headerClicked != null)
            {
                if (headerClicked == _lastHeaderClicked)
                {
                    if (_lastDirection == ListSortDirection.Ascending)
                        _lastDirection = ListSortDirection.Descending;
                    else
                        _lastDirection = ListSortDirection.Ascending;
                }
                else
                {
                    _lastDirection = ListSortDirection.Ascending;
                }

                switch (headerClicked.Content)
                {
                    case "CODE":
                        if (_lastDirection == ListSortDirection.Ascending){
                            var pList = ( from p in productList
                                        orderby p.Id
                                        select p).ToList();
                            productList = pList;
                        }
                        else{
                            var pList = (from p in productList
                                        orderby p.Id descending
                                        select p).ToList();
                            productList = pList;
                        }

                        break;
                    case "NAME":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in productList
                                         orderby p.Name
                                         select p).ToList();
                            productList = pList;
                        }
                        else
                        {
                            var pList = (from p in productList
                                         orderby p.Name descending
                                         select p).ToList();
                            productList = pList;
                        }
                        break;
                    case "ON HAND":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in productList
                                         orderby p.OnHand
                                         select p).ToList();
                            productList = pList;
                        }
                        else
                        {
                            var pList = (from p in productList
                                         orderby p.OnHand descending
                                         select p).ToList();
                            productList = pList;
                        }
                        break;
                    case "PRICE":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in productList
                                         orderby p.Price
                                         select p).ToList();
                            productList = pList;
                        }
                        else
                        {
                            var pList = (from p in productList
                                         orderby p.Price descending
                                         select p).ToList();
                            productList = pList;
                        }
                        break;
                    case "CATEGORY":
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            var pList = (from p in productList
                                         orderby p.Category
                                         select p).ToList();
                            productList = pList;
                        }
                        else
                        {
                            var pList = (from p in productList
                                         orderby p.Category descending
                                         select p).ToList();
                            productList = pList;
                        }
                        break;
                }

                lvProduct.ItemsSource = productList;
                _lastHeaderClicked = headerClicked;
            }
        }
    }
}
