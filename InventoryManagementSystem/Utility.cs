﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagementSystem
{
    public class Utility
    {
        private static readonly string LOGIN_ID = "admin";
        private static readonly string LOGIN_PWD = "12345";

        public enum CATEGORY { All, Desktops, Laptops, Printers, Monitors, Keyboards };

        public static bool IsLoggedIn = false;

        public static MainWindow mainWindow;

        public static bool Login(string id, string password)
        {
            if (id != LOGIN_ID || password != LOGIN_PWD)
            {
                return false;
            }

            IsLoggedIn = true;
            return true;
        }


    }
}
