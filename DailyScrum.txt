DAILY SCRUM - EVERY DAY AT 9AM
===============================

Each member writes down answers to 3 questions :
 

<< current date goes here >>
1. What I have done since last Scrum ?
2. What is planned from this scrum to next? (next 24 hours)
3. Where do I need assistance? What do I need to figure out?
===============================================================
example >
2018/02/28 Gregory
1. Done since last crum :
- created project plan
- created database mockup
- did not write the description yet
2. Plan until next Scrum
- finish the description
- setup the initial project
3. Need assistance / figure things out
- find and test a library for PDF print

- - - - - - - - - - - - - - - - - - - - 
2018/03/26 Valini
1. Done since last crum :
- created project plan
- created database mockup
- worked on project proposal
2. Plan until next Scrum
- create an online database for mysql
- setup the initial project
3. Need assistance / figure things out

- - - - - - - - - - - - - - - - - - - - 
2018/03/26 Suim
1. Done since last crum :
- created project and drew windows
- designed database structure
- researched the chart, exporting to pdf, exporting to excel libraries
- worked on project proposal
2. Plan until next Scrum
- to be confirmed proposal.
- setup the initial project
3. Need assistance / figure things out
- How to send a email in C# project?
- Can we use a cell phone as a QR code scanner?

- - - - - - - - - - - - - - - - - - - - 
2018/03/27 Suim
1. Done since last crum :
- studied about gridsplitter
- tested main window with gridsplitter
- created Database class and tested to connect MySQL in gear host(cloud)
- created login window and implemented partially
2. Plan until next Scrum
- complete login window
- productManage window
3. Need assistance / figure things out
- study more and practice gridsplitter

- - - - - - - - - - - - - - - - - - - - 
2018/03/27 Valini
1. Done since last crum :

-Updating database
-Creating suppliers window
2. Plan until next Scrum
- complete suppliers window
- start Manage sales window
3. Need assistance / figure things out
-  research gridsplitter UI
- - - - - - - - - - - - - - - - - - - - 
2018/03/28 Suim
1. Done since last crum :
- completed login window
- created and implemented productManage window
- implemented purchaseWindow
- created database trigger
2. Plan until next Scrum
- study tab 
- study user control
- change mainwindow into tab based
- complete productManage window
- implement send email for requesting to purchase
- study draw chart and create window
3. Need assistance / figure things out


- - - - - - - - - - - - - - - - - - - - 
2018/03/28 Valini
1. Done since last crum :
- Complete Suppliers window
-  research user control and tab control
- Adjust database
2. Plan until next Scrum
- Complete and test suppliers window
-Implement export to Excel
-Create trigger for sales
3. Need assistance / figure things out
- how to create trigger for sales
- - - - - - - - - - - - - - - - - - - - 
2018/03/28 Valini
1. Done since last crum :
- Completed Suppliers window
-  researched user control and tab control
2. Plan until next Scrum
- Work on manage sales window
-Create trigger for sales in database
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
2018/03/29 ~ 2018/04/02 Suim
1. Done since last crum :
- studied tab and user control
- changed mainwindow into tab based
- completed productManage control
- implemented send email for requesting to purchase
- studies draw chart
- implemented preference window using chart
- bug fixed for ManageSalesControl
2. Plan until next Scrum
- Study iTextSharp pdf
- Test
- Bug fix
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
2018/03/29 ~ 2018/04/02 Valini
1. Done since last crum :
- researching on export to excel from listview/datagrid
- working on manage sales
2. Plan until next Scrum
- get the export to excel working
3. Need assistance / figure things out
-Excel.Interop
- - - - - - - - - - - - - - - - - - - - 
2018/03/03  Valini
1. Done since last crum :
- researching on export to excel from listview/datagrid
-researching pdf
- manage sales
2. Plan until next Scrum
- Pdf printing
3. Need assistance / figure things out
-Excel.Interop
- - - - - - - - - - - - - - - - - - - - 
2018/03/03  Suim
1. Done since last crum :
- bug fix ManageSalesControl
- test and reorganized the windows
- researching BusyIndicator - Extended WPF Toolkit
2. Plan until next Scrum
- Add BusyIndicator into our project
3. Need assistance / figure things out
-
- - - - - - - - - - - - - - - - - - - - 
2018/03/04 Valini
1. Done since last crum :
- fixed some bugs on project
- researching printing iTextSharp
- entering new data in database
2. Plan until next Scrum
- Pdf printing
3. Need assistance / figure things out
- Pdf printing
-Excel.Interop
- - - - - - - - - - - - - - - - - - - - 
2018/03/04  Suim
1. Done since last crum :
- Added BusyIndicator into MainWindow
- TabControl delete button image bug fix
- Sales Preferences chart bug fix
2. Plan until next Scrum
- Add BusyIndicator into our project(all windows)
- logout
- Test and bug fix
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
2018/03/05 Valini
1. Done since last crum :
- fixed some bugs on project
- testing
- implemented pdf printing
2. Plan until next Scrum
- Pie chart
3. Need assistance / figure things out
- Oxyplot
-Excel.Interop
- - - - - - - - - - - - - - - - - - - - 
2018/03/05  Suim
1. Done since last crum :
- logout
- Test and bug fix
2. Plan until next Scrum
- Test and bug fix
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
2018/03/06 Valini
1. Done since last crum :
- fixed some bugs on project
- Fixed pdf layout itextSharp
- implemented pdf printing
2. Plan until next Scrum
- Keeping a record of customer receipt
3. Need assistance / figure things out
-Excel.Interop
- - - - - - - - - - - - - - - - - - - - 
2018/03/06 - 2018/03/08  Suim
1. Done since last crum :
- Implemented to make PDF Filename different for every case
- Implemented sorting listview code when click title of grid in ProductManageControl & SupplierManageControl
- Test and bug fix
2. Plan until next Scrum
- Test and bug fix
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
<<<<<<< HEAD
2018/03/06 - 2018/03/08  Valini
1. Done since last crum :
- Changed button functionality of some windows to simplify 
- Test Excel and bug fix
2. Plan until next Scrum
- Test and bug fix
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
 2018/03/09  Valini
1. Done since last crum :
- Made presentation ppt
- Test and bug fix
2. Plan until next Scrum
- Test and bug fix
- - - - - - - - - - - - - - - - - - - - 
2018/03/09  Suim
1. Done since last crum :
- Test and bug fix
- Prepared presentation
2. Plan until next Scrum
- Prepare presentation
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
2018/03/10  Valini
1. Done since last crum :
- Test and bug fix
- Preparing for presentation
2. Plan until next Scrum
- Prepare presentation
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
2018/03/10  Suim
1. Done since last crum :
- Test and bug fix
- Prepared presentation
2. Plan until next Scrum
- Prepare and practice presentation
3. Need assistance / figure things out
- - - - - - - - - - - - - - - - - - - - 
2018/03/11  Suim
1. Done since last crum :
- Test and bug fix
- Preparing for presentation
2. Plan until next Scrum
- Prepare and practice presentation
3. Need assistance / figure things out
2018/03/11 Valini
1. Done since last crum :
- Test and bug fix
- Preparing for presentation
2. Plan until next Scrum
- Prepare and practice presentation
3. Need assistance / figure things out